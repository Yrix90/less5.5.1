terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
}

variable "cloudflare_email" {
    description = "cloudflare email"
}
variable "cloudflare_api_key" {
    description = "cloudflare api_key"
}
variable "cloudflare_account_id" {
    description = "cloudflare account_id"
}

provider "cloudflare" {
    email = "${var.cloudflare_email}"
    api_key = "${var.cloudflare_api_key}"
    account_id = "${var.cloudflare_account_id}"
}

provider "yandex" {
  service_account_key_file = file("terraform-ya-key.json")
  cloud_id  = "b1gu1kbkrj1334nml4cc"
  folder_id = "b1ga3koa2icjbb6jg0ja"
  zone      = "ru-central1-a"
}