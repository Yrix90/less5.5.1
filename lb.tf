resource "yandex_alb_target_group" "target-group" {
  name      = "junway-target-group"

  target {
    subnet_id = "${yandex_vpc_subnet.default.id}"
    ip_address   = "${yandex_compute_instance.vm_instance.0.network_interface.0.ip_address}"
  }

  target {
    subnet_id = "${yandex_vpc_subnet.default.id}"
    ip_address   = "${yandex_compute_instance.vm_instance.1.network_interface.0.ip_address}"
  }
}

resource "yandex_alb_backend_group" "junway-backend-group" {
  name      = "junway-backend-group"

  http_backend {
    name = "junway-http-backend"
    weight = 1
    port = 8000
    target_group_ids = ["${yandex_alb_target_group.target-group.id}"]
    load_balancing_config {
      panic_threshold = 100
    }    
    healthcheck {
      timeout = "1s"
      interval = "1s"
      http_healthcheck {
        path  = "/"
      }
    }
  }
}

resource "yandex_alb_http_router" "tf-router" {
  name      = "junway-http-router"
}

resource "yandex_alb_virtual_host" "junway-virtual-host" {
  name      = "junway-virtual-host"
  http_router_id = yandex_alb_http_router.tf-router.id
  route {
    name = "junway-route"
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.junway-backend-group.id
        timeout = "3s"
      }
    }
  }
}

resource "yandex_alb_load_balancer" "junway-balancer" {
  name        = "junway-load-balancer"

  network_id  = yandex_vpc_network.default.id

  allocation_policy {
    location {
      zone_id   = "ru-central1-a"
      subnet_id = yandex_vpc_subnet.default.id 
    }
  }

  listener {
    name = "junway-listener"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80 ]
    }    
    http {
      handler {
        http_router_id = yandex_alb_http_router.tf-router.id
      }
    }
  }    
}