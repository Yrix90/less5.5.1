variable "node_count" {
    default = 2
}
variable "pub_key_path" {
    description = "pub key"
}
variable "private_key_path" {
    description = "priv key"
}

resource "yandex_vpc_network" "default" {}

resource "yandex_vpc_subnet" "default" {
  v4_cidr_blocks = ["10.2.0.0/16"]
  zone       = "ru-central1-a"
  network_id = "${yandex_vpc_network.default.id}"
}

resource "yandex_compute_instance" "vm_instance" {
  count = "${var.node_count}"
  name = "node-${count.index+1}"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      size = 10
      image_id = "fd8tf620cag322arr3n5"
    }
  }

  network_interface {
    subnet_id = "${yandex_vpc_subnet.default.id}"
    nat = true
  }

  connection {
      type = "ssh"
      user = "debian"
      private_key = "${file(var.private_key_path)}"
      timeout = "1m"
      host = "${self.network_interface.0.nat_ip_address}"
    }
  provisioner "remote-exec" {
      inline = [
          "sudo apt update",
          # "sudo apt install ca-certificates curl gnupg lsb-release -y",
          # "sudo apt install docker-ce docker-ce-cli containerd.io -y",
          # "curl -fsSL https://get.docker.com -o get-docker.sh",
          # "sudo sh ./get-docker.sh",
          # "sudo groupadd docker",
          # "sudo usermod -aG docker $USER",
          # "sudo echo '<html><body><h1>Juneway ${self.network_interface.0.nat_ip_address} ${self.name}</h1></body></html>' > ~/index.html",
          # "sudo docker run -d --name nginx -v $HOME/index.html:/usr/share/nginx/html/index.html -p 80:80 nginx"
      ]
  }
  provisioner "local-exec" {
    command = "ansible-playbook -u debian -i '${self.network_interface.0.nat_ip_address},' --private-key ${var.private_key_path} --ssh-common-args '-o StrictHostKeyChecking=no' ./ansible/playbooks/deploy_app.yml"
  }

  metadata = {
    ssh-keys = "debian:${file(var.pub_key_path)}"
  }

}