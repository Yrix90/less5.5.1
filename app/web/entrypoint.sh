#!/bin/sh

echo "Waiting..."

while ! nc -z db 5432; do
    sleep 0.1
done

echo "Postgres started"

python manage.py flush --no-input
python manage.py migrate


exec "$@"