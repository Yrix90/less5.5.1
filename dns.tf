resource "cloudflare_record" "yrix90" {
    zone_id = "38a3f690d0be85a323ae19af19fd01e8"
    name = "yrix90.juneway.pro"
    value = "${yandex_alb_load_balancer.junway-balancer.listener.0.endpoint.0.address.0.external_ipv4_address.0.address}"
    type = "A"
    ttl = 3600  
}